#include <xdo.h>
#include "mouse.h"

void get_mouse(int *x, int *y, int *screen_num)
{
    xdo_t *xdo = xdo_new(NULL);

    xdo_mouselocation(xdo, x, y, screen_num);

    xdo_free(xdo);
}
