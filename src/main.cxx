#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>

#include <glib.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "mouse.h"

#define BUFF_SIZE (100)
#define SLEEP_US (1000000 / 60)

bool poll = FALSE;

void sigterm_handler(int signal)
{
    (void)signal;

#if DEBUG
    printf("caught SIGTERM");
#endif

    if(poll)
        printf("\n");

    exit(EXIT_SUCCESS);
}

int poll_get_color(Display *d, int prev_chars)
{
    XColor c;
    XImage *image;
    int x, y, screen_num;

    get_mouse(&x, &y, &screen_num);

    image = XGetImage(d, RootWindow(d, DefaultScreen(d)), x, y, 1, 1, AllPlanes, XYPixmap);
    c.pixel = XGetPixel(image, 0, 0);
    XFree(image);
    XQueryColor(d, DefaultColormap(d, DefaultScreen(d)), &c);

    fflush(stdout);

    char buff[BUFF_SIZE];

    if(prev_chars)
    {
        memset(buff, '\b', BUFF_SIZE-1);
        buff[prev_chars] = 0;
        printf("%s", buff);
    }

    memset(buff, ' ', BUFF_SIZE-1);
    buff[BUFF_SIZE-1] = 0;
    sprintf(buff, "\r[r, g, b] = [%3d, %3d, %3d]", c.red / 256, c.green / 256, c.blue / 256);

    return printf("%s", buff);
}

void show_help(char *name)
{
    printf("Usage: %s [options]\n", name);
    printf("    -h || --help    Display the help dialog\n");
    printf("    -p || --poll    Poll for color\n");
}

int main(int argc, char *argv[])
{
    Display *d;
    bool do_show_help = FALSE;

    signal(SIGTERM, &sigterm_handler);
    signal(SIGINT, &sigterm_handler);

    for(int i = 1; i < argc; i++)
    {
        if(0 == strcmp("-h", argv[i]) || 0 == strcmp("--help", argv[i]))
        {
#ifdef DEBUG
            printf("will display help\n");
#endif
            do_show_help = TRUE;
        }
        else if(0 == strcmp("-p", argv[i]) || 0 == strcmp("--poll", argv[i]))
        {
#ifdef DEBUG
            printf("will poll\n");
#endif
            poll = true;
        }
        else
        {
            fprintf(stderr, "Unrecognized option: %s\n", argv[i]);
        }
    }

    if(do_show_help)
    {
        show_help(argv[0]);
        exit(EXIT_FAILURE);
    }

    d = XOpenDisplay(NULL);

    if(!poll)
    {
        poll_get_color(d, 0);
        printf("\n");
    }
    else
    {
        int prev_chars = 0;
        while(1)
        {
            prev_chars = poll_get_color(d, prev_chars);

            g_usleep(SLEEP_US);
        }
    }

    return 0;
}
