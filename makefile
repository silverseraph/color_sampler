cflags = $(shell pkg-config --cflags glib-2.0) -O3
libs = $(shell pkg-config --libs glib-2.0) -lm -lxdo -lX11
debug =

all: clean build

clean:
	rm -rf obj

debug:
	make build debug=-DDEBUG

build:
	mkdir -p obj
	mkdir -p bin
	gcc ./src/mouse.c -c -o ./obj/mouse.o ${cflags} ${debug}
	g++ ./src/main.cxx -c -o ./obj/main.o ${cflags} ${debug}
	g++ ./obj/mouse.o ./obj/main.o ${cflags} ${libs} ${debug} -o bin/color_sample

install:
	install -m 755 bin/color_sample /usr/bin/color_sample
